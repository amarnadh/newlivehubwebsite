const express = require("express");
const connection = require("../database/index");
const router = express.Router();

/* signup verification */
/*router.post("/", function (req, res, next) {
  connection.db.collection("logs").then((collection) => {
    var logs = {};
    logs = req.body.logs;
    connection.insertData(logs, "logs");
    res.json({ hi: "giygi" });
  });
});
*/
router.post("/windows", function (req, res, next) {
  res.download("README.md");
  let date_ob = new Date();

  // current date
  // adjust 0 before single digit date
  let date = ("0" + date_ob.getDate()).slice(-2);

  // current month
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  // current year
  let year = date_ob.getFullYear();

  // current hours
  let hours = date_ob.getHours();

  // current minutes
  let minutes = date_ob.getMinutes();

  // current seconds
  let seconds = date_ob.getSeconds();

  // prints date in YYYY-MM-DD format

  // prints date & time in YYYY-MM-DD HH:MM:SS format
  var logs = {};
  logs.log =
    "A windows file is dowload at " +
    year +
    "-" +
    month +
    "-" +
    date +
    " " +
    hours +
    ":" +
    minutes +
    ":" +
    seconds;
  connection.insertData(logs, "logs");
  // prints time in HH:MM format

  console.log(logs);
  //connection.insertData(log);
});
router.post("/linux", function (req, res, next) {
  res.download("README.md");
  let date_ob = new Date();

  // current date
  // adjust 0 before single digit date
  let date = ("0" + date_ob.getDate()).slice(-2);

  // current month
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  // current year
  let year = date_ob.getFullYear();

  // current hours
  let hours = date_ob.getHours();

  // current minutes
  let minutes = date_ob.getMinutes();

  // current seconds
  let seconds = date_ob.getSeconds();

  // prints date in YYYY-MM-DD format

  // prints date & time in YYYY-MM-DD HH:MM:SS format
  var log = {};
  log.log =
    "A linux file is dowload at " +
    year +
    "-" +
    month +
    "-" +
    date +
    " " +
    hours +
    ":" +
    minutes +
    ":" +
    seconds;
  connection.insertData(log, "logs");
  // prints time in HH:MM format

  console.log(log);
  //connection.insertData(log);
});
router.post("/mac", function (req, res, next) {
  res.download("testinvite.rest");
  let date_ob = new Date();

  // current date
  // adjust 0 before single digit date
  let date = ("0" + date_ob.getDate()).slice(-2);

  // current month
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  // current year
  let year = date_ob.getFullYear();

  // current hours
  let hours = date_ob.getHours();

  // current minutes
  let minutes = date_ob.getMinutes();

  // current seconds
  let seconds = date_ob.getSeconds();

  // prints date in YYYY-MM-DD format

  // prints date & time in YYYY-MM-DD HH:MM:SS format
  var logs = {};
  logs.log = "A mac file is downloaded at";
  year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
  connection.insertData(logs, "logs");
  // prints time in HH:MM format

  console.log(logs);
  //connection.insertData(log);
});

module.exports = router;
