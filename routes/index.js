const express = require("express");
const router = express.Router();
const connection = require("../database/index");

const superAPI = require("../routes/super");
const downloadAPI = require("../routes/download");

connection.createCollections();
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.use("/super", superAPI);
router.use("/download", downloadAPI);
module.exports = router;
