const mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    username : {
        type: String,
        trim: true,
        required : "Required"
    },
    userId: {
        type: String,
        required: "Required"
    },
    status : {
        type : Boolean,
        default: true,
        required : "Required"
    },
    S_ID: {
        type: String,
        required: "Required",
        trim: true
    }
});

mongoose.model("users",userSchema)