const mongoose = require('mongoose');

var userMetaDataSchema = new mongoose.Schema({
    username : {
        type: String,
        trim: true,
        required : "Required"
    },
    userId: {
        type: String,
        required: "Required"
    },
   
});

mongoose.model("userMetaData",userMetaDataSchema)