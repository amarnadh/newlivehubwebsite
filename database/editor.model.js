//const mongoose = require('mongoose');
const mongoose = require('mongoose');

var editSchema = new mongoose.Schema({
    sender : {
        type: String,
        trim: true,
        required : "Required"
    },
    reciever: {
        type: String,
        trim: true,
        required: "Required"
    },
    loc : {
        type : Number,
        required : "Required"
    },
    lastUpdatedTime: {
        type: String,
        required: "Required",
        trim: true
    },
    content:{
        type: String,
        required: "Required",
        trim: false
    }
});

mongoose.model("edit",editSchema)