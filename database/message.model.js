const mongoose = require('mongoose');

var messageSchema = new mongoose.Schema({
    sender : {
        type: String,
        trim: true,
        required : "Required"
    },
    reciever: {
        type: String,
        trim: true,
        required: "Required"
    },
    message : {
        type : String,
        trim: false,
        required : "Required"
    },
    isNewMsg : {
        type: Boolean,
        required: "Required"
    },
    time: {
        type: Date,
        required: "Required"
    }
});

mongoose.model("message",messageSchema)