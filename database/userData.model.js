const mongoose = require('mongoose');

var userDataSchema = new mongoose.Schema({
    username: {
        type: String,
        trim: true,
        required : "Required"
    },
    password: {
        type: String,
        trim: true,
        required: "Required"
    },
    userId: {
        type: String,
        required: "Required"
    },
    createdTime: {
        type: Date,
        required: "Required"
    },
    picUrl: {
        type: String
    }
});

mongoose.model("userData",userDataSchema)