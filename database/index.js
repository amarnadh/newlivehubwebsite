const fs = require("fs");
const mkdirp = require("mkdirp");
const path = require("path");
const FileSystemStore = require("file-system-store").FileSystemStore;
const MongoPortable = require("mongo-portable").MongoPortable;
const db = new MongoPortable("livehub");
const DB_DIR = "/home/.livehub/db";
const DDBB_PATH = path.normalize(DB_DIR);
db.addStore(
  new FileSystemStore({
    // The path where the database will be stored
    ddbb_path: DDBB_PATH,
    // Whether the persistance will be asynchronous or not
    sync: false,
  })
);

exports.db = db;
exports.insertData = function (data, collection) {
  db.collection(collection).then((collection) => {
    collection.insert(data).then((document) => {});
  });
};

exports.createCollections = function () {
  mkdirp(path.normalize(DB_DIR + "/livehub"), (err) => {
    if (!err) {
      var collections = [
        "users",
        "projects",
        "instances",
        "editor",
        "chats",
        "requests",
        "reports",
        "mails",
        "admin",
        "guest",
        "index",
        "todo",
        "survey",
        "files",
        "channels",
        "storage",
        "notifications",
        "poll",
        "logs",
      ];
      collections.forEach((collection) => {
        fs.openSync(
          path.normalize(DB_DIR + "/livehub/" + collection + ".json"),
          "a"
        );
      });
      console.log("database connection success!");
    }
  });
};
